---
author: "Mario García"
title: "Configura tu propio GitLab Runner"
date: "2022-04-18"
description: "Guía para configurar GitLab Runner"
tags: ["gitlab", "tutorial"]
ShowToc: true
ShowBreadCrumbs: true
---

Cuando se configura una nueva cuenta de GitLab en [GitLab.com](https://gitlab.com/), se obtiene acceso a una prueba gratuita de 30 días de GitLab Ultimate y se necesita validar la cuenta con una tarjeta de crédito para poder usar los minutos gratis de [GitLab Runner](https://docs.gitlab.com/runner/).

Para quienes les preocupa la privacidad, se puede [instalar](https://docs.gitlab.com/runner/install/) GitLab Runner en una infraestructura propia. Puede instalarse:

- En un contenedor, usando [Docker](https://docs.gitlab.com/runner/install/docker.html), [Kubernetes](https://docs.gitlab.com/runner/install/kubernetes.html) o [OpenShift](https://docs.gitlab.com/runner/install/openshift.html).
- Descargando manualmente un binario e instalándolo en [GNU/Linux](https://docs.gitlab.com/runner/install/linux-manually.html), [macOS](https://docs.gitlab.com/runner/install/osx.html), [Windows](https://docs.gitlab.com/runner/install/windows.html) o [FreeBSD](https://docs.gitlab.com/runner/install/freebsd.html).
- Desde un [repositorio](https://docs.gitlab.com/runner/install/linux-repository.html) de paquetes rmp/deb.

## Ejecutar GitLab Runner en un contenedor
Antes de instalar GitLab Runner, hay que asegurarse que Docker está ejecutándose en el sistema. En GNU/Linux se deben seguir las instrucciones en la [documentación](https://docs.docker.com/engine/install/) o ir directamente a la sección que muestra las instrucciones para cada distro:

- [Centos](https://docs.docker.com/engine/install/centos)
- [Debian](https://docs.docker.com/engine/install/debian)
- [Fedora](https://docs.docker.com/engine/install/fedora)
- [RHEL](https://docs.docker.com/engine/install/rhel/)
- [SLES](https://docs.docker.com/engine/install/sles/)
- Desde [binarios](https://docs.docker.com/engine/install/binaries/)

Se deben seguir las instrucciones [post-instalación](https://docs.docker.com/engine/install/linux-postinstall/).

### Instalar la imagen de Docker e iniciar el contenedor

Para instalar la imagen de Docker, se debe crear un volumen de Docker:

```
docker volume create gitlab-runner-config
```

Iniciar el contenedor de GitLab Runner:

```
docker run -d --name gitlab-runner --restart always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v gitlab-runner-config:/etc/gitlab-runner \
    gitlab/gitlab-runner:latest
```

### Registrar el runner

Antes de usar el recién instalado GitLab Runner, se debe [registrar](https://docs.gitlab.com/runner/register/index.html#docker).

Ir a la configuración del proyecto de GitLab en el que quieres usar GitLab Runner y obtener el token de registro. En el repositorio de GitLab, ir a **Settings** --> **CI/CD** y expandir la sección *Runners*. De la sección *Specific runners*, copiar el token de registro. Es importante deshabilitar la opción *Shared runners*.

Ahora desde la terminal, ejecutar el siguiente comando:

```
docker run --rm -it -v gitlab-runner-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register
```

Solicitará la siguiente información:

- Enter the GitLab instance URL: https://gitlab.com/
- Enter the registration token: El token que se copió anteriormente
- Enter a description for the runner: Para que se usará
- Enter tags for the runner: Las etiquetas se separan por comas y se asignan manualmente a los trabajos en los pipelines de CI/CD. Se pueden editar después.
- Enter optional maintenance note for the runner.
- Enter an executor: docker
- Enter the default Docker image: ruby:2.7. Imagen por defecto que se usará cuando no se especifique en el archivo *.gitlab-ci.yml*.

Si no se asignan las etiquetas a los trabajos en el pipeline de CI/CD, el GitLab Runner no iniciará ya que está configurado por defecto para seleccionar trabajos que tengan cualquiera de las etiquetas especificadas en la configuración.

Para cambiar este comportamiento y permitir GitLab Runner seleccione trabajos sin etiquetas, expandir la sección *Runners* desde **Settings** --> **CI/CD** y hacer clic en el botón de editar en el runner que se quiere modificar en la sección **Available specific runners**. En seguida, marcar la siguiente opción: *Indicates whether this runner can pick jobs without tags*.

Por defecto los runners están configurados para usarse solo con un proyecto específico, si se requiere que otros proyectos en la cuenta usen el mismo runner, desmarcar la opción: *When a runner is locked, it cannot be assigned to other projects*. En otros proyectos, expandir la sección *Runners* en **Settings** --> **CI/CD**, ir a *Available specific runners* y hacer clic en *Enable for this project* para elegir el runner que se usará.

GitLab CI está listo para usarse y configurar pipelines de CI/CD usando un GitLab Runner propio.
